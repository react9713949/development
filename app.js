const express = require('express');
const app = express();
const PORT = 3000;
const path = require('path');

app.get('/', (req, res) => {
  res.send('Hello, world from Express! and react');
});

app.listen(PORT, () => {
  console.log(`La aplicación está escuchando en el puerto ${PORT}`);
});

// Ruta estática para el frontend
app.use(express.static(path.join(__dirname, 'frontend/build')));

// Enviar el archivo index.html para cualquier otra ruta
app.get('*', (req, res) => {
  res.sendFile(path.join(__dirname, 'frontend/build/index.html'));
});
