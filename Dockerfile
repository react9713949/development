# Utiliza una imagen de Node.js para construir el frontend
FROM node:20-alpine AS build

WORKDIR /usr/src/app

# Instala las dependencias del backend
COPY package*.json ./
RUN npm install

# Copia los archivos del backend
COPY . .

# Construye el frontend
WORKDIR /usr/src/app/frontend
RUN npm install
RUN npm run build

# Inicia la etapa final utilizando una imagen de Nginx
FROM nginx:alpine

# Copia los archivos del frontend construidos en la etapa anterior
COPY --from=build /usr/src/app/frontend/build /usr/share/nginx/html

# Configura Nginx para redirigir el tráfico al backend
COPY nginx/default.conf /etc/nginx/conf.d/default.conf

# Exponer el puerto 80 para Nginx
EXPOSE 80

# Inicia Nginx en primer plano
CMD ["nginx", "-g", "daemon off;"]
